<?php
// PHP Math Exercises: Generate random 11 characters string of letters and numbers
// PHP math: Exercise-3 with Solution
// https://www.w3resource.com/php-exercises/php-math-exercise-3.php

$x = rand(10e12, 10e16);
echo base_convert($x, 10, 36)."\n";
?>