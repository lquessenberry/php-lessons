# PHP Math functions - Exercises, Practice, Solution
## Last update on February 26 2020 08:09:35 (UTC/GMT +8 hours)

### PHP Math functions [ 12 exercises with solution]

1. Write a PHP script to find the maximum and minimum marks from the following set of arrays. 
Sample arrays :
$marks1 = array(360,310,310,330,313,375,456,111,256);
$marks2 = array(350,340,356,330,321);
$marks3 = array(630,340,570,635,434,255,298);
Expected Output :
Maximum marks : 635
Minimum marks : 111

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-1.php)

2. Write a PHP script which rounds the following values with 1 decimal digit precision. 
Sample values and Output :
1.65 --> 1.7
1.65 --> 1.6
-1.54 --> -1.5
Click me to see the solution

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-2.php)

3. Write a PHP script to generate random 11 characters string of letters and numbers. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-3.php)

4. Write a PHP script to convert scientific notation to an int and a float. 
Sample scientific notation : 4.5e3
Expected Output : 4 & 4500

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-4.php)

5. Write a PHP script to convert a date from yyyy-mm-dd to dd-mm-yyyy. 
Sample floating value : 0.0456
Expected Output :
Exponent part : -4
Mantissa part : 0.7296

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-5.php)

6. Write a PHP script to get the information regarding memory usage in KB or MB etc. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-6.php)

7. Find earliest and latest dates from a list of dates. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-7.php)

8. Write a PHP function to round a float away from zero to a specified number of decimal places. 
Sample Data :
(78.78001, 2)
(8.131001, 2)
(0.586001, 4)
(-.125481, 3)
-.125481
Sample Output :
78.79
8.14
0.5861
-0.126
-1

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-8.php)

9. Write a PHP function to convert Arabic Numbers to Roman Numerals. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-9.php)

10. Write a PHP function to get random float numbers. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-10.php)

11. Write a PHP function to create a human-readable random string for a captcha. 
Sample Output :
hoboh
tynzh

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-11.php)

12. Write a PHP function to get the distance between two points on the earth. 

[Click me to see the solution](https://www.w3resource.com/php-exercises/php-math-exercise-12.php)

